# WAR-TRIANGLE

## Problem Statement
A war was declared between the Spartan and the Egyptian allies during 404 BC. The
spartan king selected a group of soldiers and ordered them to form two right angled
triangle shaped groups of soldiers to counter attack the Bow shaped enemy soldiers
approaching. A program which will help the king to form the right angled triangle
when he knows how many rows with which the triangle is to be formed.